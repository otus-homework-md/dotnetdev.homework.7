using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("customers")] 
    public class CustomerController : Controller
    {
        CustomerContext db;
        public CustomerController(CustomerContext context)
        {
            db = context;
            if (!db.Customers.Any())
            {
                db.Customers.Add(new Customer {  Firstname="Tom", Lastname="Dope" });
                db.Customers.Add(new Customer { Firstname="Nik", Lastname="Pick" });
                db.SaveChanges();
            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Customer>>> Get()
        {
            return await db.Customers.ToListAsync();
        }

        [HttpGet("{id:long}")]   
        public async Task<ActionResult<Customer>> GetCustomerAsync([FromRoute] long id)
        {
            Customer user = await db.Customers.FirstOrDefaultAsync(x => x.Id == id);
            if (user == null)
                return NotFound();
            return new ObjectResult(user);
        }

        [HttpPost("")]   
        public async Task<ActionResult<Customer>> CreateCustomerAsync([FromBody] Customer customer)
        {
             if (customer == null)
            {
                return BadRequest();
            }
 
            db.Customers.Add(customer);
            await db.SaveChangesAsync();
            return Ok(customer);
        }

        // PUT api/users/
        [HttpPut]
        public async Task<ActionResult<Customer>> Put(Customer user)
        {
            if (user == null)
            {
                return BadRequest();
            }
            if (!db.Customers.Any(x => x.Id ==user.Id))
            {
                return NotFound();
            }
 
            db.Update(user);
            await db.SaveChangesAsync();
            return Ok(user);
        }
 
        // DELETE api/users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Customer>> Delete(int id)
        {
            Customer user = db.Customers.FirstOrDefault(x => x.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            db.Customers.Remove(user);
            await db.SaveChangesAsync();
            return Ok(user);
        }


    }
}