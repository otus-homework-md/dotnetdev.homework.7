﻿using WebApi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace WebApi.Models
{
    public class CustomerContext:DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        /// <summary>
        /// Конструктор контекста
        /// </summary>
        public CustomerContext(DbContextOptions<CustomerContext> options): base(options)
        {
            Database.EnsureCreated();
        }
 
        ///// <summary>
        ///// Подключаемся к БД
        ///// </summary>
        ///// <param name="optionsBuilder"></param>
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
            
        //    ConfigurationBuilder builder = new ConfigurationBuilder();
        //    builder.SetBasePath(Directory.GetCurrentDirectory());
        //    builder.AddJsonFile("appconfig.json");
        //    IConfigurationRoot config = builder.Build();

        //    string connectionString = config.GetConnectionString("DefaultConnection");

        //    optionsBuilder.UseNpgsql(connectionString);

        //    if (EnableLogging)
        //        optionsBuilder.LogTo(System.Console.WriteLine);

        //}

        ///// <summary>
        ///// Создание БД и наполнение первичными данными при необходимости
        ///// </summary>
        ///// <param name="modelBuilder"></param>
        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{

        //    Customer[] customers =new Customer[] 
        //    {
        //        new Customer {Id=1,  Firstname="Tom", Lastname="Dope"},
        //        new Customer {Id=2,  Firstname="Alice", Lastname="Cook"},
        //        new Customer {Id=3,  Firstname="Sam", Lastname="Spoke"},
        //        new Customer {Id=4,  Firstname="Ben", Lastname="Tell"},
        //        new Customer {Id=5,  Firstname="Nik", Lastname="Pick"}
        //    };


        //    modelBuilder.Entity<Customer>().HasData(customers);


    
        //}

    }
}
