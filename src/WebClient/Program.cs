﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using System.Configuration;

namespace WebClient
{
    static class Program
    {

        static readonly HttpClient _httpClient = new ();
        static readonly string _url = ConfigurationManager.AppSettings.Get("url") ?? "https://localhost:5001/customers";

        static async Task Main(string[] args)
        {
            do
            {
                //получение данных клента по ID
                Console.WriteLine("Enter client ID:");
                var input = Console.ReadLine();
                if (long.TryParse(input, out var _))
                {
                    var customer = await GetAsync(input);
                    Console.WriteLine($"Client from database: {customer}");
                }
                else
                {
                    Console.WriteLine("Incorrect client ID");
                }

                //отправка данных случайным образом сгенерированного клиента
                var postResponse = await _httpClient.PostAsJsonAsync(_url, RandomCustomer());
                try
                {
                    long idCustomer =JsonSerializer.Deserialize<Customer>( await postResponse.Content.ReadAsStringAsync()).Id;
                    var cust = await GetAsync(idCustomer.ToString());
                    Console.WriteLine($"Random customer was created: {cust}");
                }
                catch
                {

                }
                


                Console.WriteLine("Enter q to exit, or any to continue:");
                if ("q".Equals(Console.ReadLine())) { break; }
            }
            while (true);
  
        }

       
        /// <summary>
        /// генерация случайного пользователя
        /// </summary>
        /// <returns></returns>
        private static CustomerCreateRequest RandomCustomer()
        {
            return new CustomerCreateRequest { Firstname = Faker.Name.First(), Lastname = Faker.Name.Last()  };
        }


        /// <summary>
        /// Получаем пользователя из БД по ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private static async Task<Customer> GetAsync(string? id)
        {
            var getResponse =  await _httpClient.GetAsync($"{_url}/{id}");
            var resultResponse = await getResponse.Content.ReadAsStringAsync();
            if (getResponse == null || getResponse.StatusCode != HttpStatusCode.OK)
            {
                Console.WriteLine(resultResponse);
                return null;
            }
            return  JsonSerializer.Deserialize<Customer>(resultResponse);
        }

    }
}